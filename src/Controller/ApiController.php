<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Entity\Beer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Services\ApiService;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/{food}", name="api_food")
     * @Method("GET")
     */
    public function apiFood($food, ApiService $apiService)
    {
        $datos = $apiService->getBeers();
        if (!$datos) {
            throw new HttpException(500, 'La obtención de datos no se ha realizado con éxito');
        } else {
            $beers = json_decode($datos,true);
            $beersObjects = $this->newBeers($beers);
            $filterBeersFood = $apiService->filterBeers($beersObjects,$food);
        }
        $response = new JsonResponse();
        $beers = array();
        foreach($filterBeersFood as $beer) {
            $resultado = array(
                "ID" => $beer->getID(),
                "Nombre" => $beer->getNombre(),
                "Descripcion" => $beer->getDescripcion()
            );
            array_push($beers,$resultado);
        }
        if (empty($beers)) {
            $response->setData(array ("Not Found" => "No se han encontrado resultados"));
        } else {
            $response->setData($beers);
        }
        return $response;

    }

    /**
     * @Route("/api/full/{food}", name="api_food_full")
     * @Method("GET")
     */
    public function apiFoodFull($food, ApiService $apiService)
    {
        $datos = $apiService->getBeers();
        if (!$datos) {
            throw new HttpException(500, 'La obtención de datos no se ha realizado con éxito');        
        } else {
           $beers = json_decode($datos,true);
           $beersFormated = $this->newBeers($beers);
           $filterBeersFood = $apiService->filterBeers($beersFormated, $food);
        }
        
        $response = new JsonResponse();
        $beers = array();
        foreach($filterBeersFood as $beer) {
            $resultado = array(
                "ID" => $beer->getID(),
                "Nombre" => $beer->getNombre(),
                "Descripcion" => $beer->getDescripcion(),
                "Imagen" => $beer->getImagen(),
                "Slogan" => $beer->getSlogan(),
                "Fecha de Fabricacion" => $beer->getFechaFabricacion()
            );
            array_push($beers,$resultado);
        }
        if (empty($beers)) {
            $response->setData(array ("Not Found" => "No se han encontrado resultados"));
        } else {
            $response->setData($beers);
        }
        return $response;

    }

    public function newBeers($beers)
    {
        $arrayBeers = array();
        foreach ($beers as $beer) {
            array_push($arrayBeers, new Beer($beer["id"], $beer["name"], $beer["description"], $beer["image_url"], $beer["tagline"], $beer["first_brewed"], $beer["food_pairing"]) );
        }
        return $arrayBeers;
    } 

}
