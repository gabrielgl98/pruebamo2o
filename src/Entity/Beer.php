<?php

namespace App\Entity;

class Beer
{
    private $id;

    private $nombre;

    private $descripcion;

    private $imagen;

    private $slogan;

    private $fechaFabricacion;

    private $food;

    public function __construct($id,$nombre,$descripcion,$imagen,$slogan,$fechaFabricacion,$food) {

        $this->id = $id;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->imagen = $imagen;
        $this->slogan = $slogan;
        $this->fechaFabricacion = $fechaFabricacion;
        $this->food = $food;

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function getSlogan(): ?string
    {
        return $this->slogan;
    }

    public function getFechaFabricacion(): ?string
    {
        return $this->fechaFabricacion;
    }

    public function getFood()
    {
        return $this->food;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }
}
