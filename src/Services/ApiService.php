<?php
namespace App\Services;

class ApiService
{
    public function filterBeers($beersFormated, $food)
    {
        $beersMatched = array();
        foreach($beersFormated as $beer) {
            $match = array_filter($beer->getFood(), function ($var) use ($food) {
                    return stristr($var, $food);
                }
            );
            if ($match) {
                array_push($beersMatched, $beer);
            }
        }
        return $beersMatched;
        
    }

    public function getBeers()
    {
        $client = new \GuzzleHttp\Client( array(
			'curl'            => array( CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false ),
			'allow_redirects' => false,
			'cookies'         => true,
			'verify'          => false
		));
        $respuesta = $client->request('GET', 'https://api.punkapi.com/v2/beers');
        
        return $respuesta->getBody();
    }
}
